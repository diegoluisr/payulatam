# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')

import os
import re

def getNamespace(file):
  dirname = os.path.dirname(__file__)
  parts = ('PayU' + os.path.abspath(dirname + file).replace(dirname, '')).split('/')
  del parts[-1]
  return ('\\'.join(parts)).replace('\\src', '')

def getClassname(file):
  dirname = os.path.dirname(__file__)
  parts = ('PayU' + os.path.abspath(dirname + file).replace(dirname, '')).split('/')
  return str(parts[len(parts) - 1]).replace('.php', '')

def analyzeFolder(folder, ignoreList):
  directory = os.path.dirname(folder)
  sourcesList = []

  for currentpath, dirs, files in os.walk('.'):
    for file in files:
      filename = os.path.join(currentpath, file)
      if filename.find('.php') > 0:
        fileName = filename.replace("./", "/")
        sourcesList.append(
          {
            'filename': fileName,
            'package': getNamespace(fileName),
            'classname': getClassname(fileName),
            'fullname': getNamespace(fileName) + "\\" + getClassname(fileName),
            'uses': getClassesInFile(filename, ignoreList)
          }
        )
  return sourcesList

def getClassesInFile(fileName, ignoreList):
  classesList = []
  with open(fileName, 'r') as file:
    for line in file:
      matches = re.findall("(extends ([A-Z][a-z]*)+)|(new ([A-Z][a-z]*)+)|(([A-Z][a-z]*)+::)|(stdClass)", line)
      if len(matches) == 1:
        for match in list(matches[0]):
          if match.find('new ') or match.find('::'):
            classItem = match.replace('extends ', '').replace('new ', '').replace('::', '')
            if len(classItem) > 1:
              className = match.replace('extends ', '').replace('new ', '').replace('::', '')
              if className not in ignoreList:
                classesList.append(className)
    file.close()
  classesList = list(dict.fromkeys(classesList))
  return classesList

def getClassUses(classList):
  for idx, classItem in enumerate(classList):
    finalUses = []
    for classUse in classItem.get('uses'):
      broken = False
      for classItemCompare in classList:
        if classUse == classItemCompare.get('classname'):
          finalUses.append(classItemCompare.get('fullname'))
          broken = True
          break
      if broken == False:
        finalUses.append(classUse)
    classList[idx]['uses'] = finalUses
  return classList


def updateFiles(mapping):

  for item in mapping:
    dirname = os.path.dirname(__file__)
    filename = os.path.join(dirname, item.get('filename').replace('/', '', 1))
    
    newLines = []
    with open(filename, 'r') as file:
      counter = 0
      for line in file:
        if counter == 1:
          if line.find('namespace') == -1:
            newLines.append("namespace " + item.get('package') + ";")
            newLines.append("\n")
            newLines.append("\n")
            for use in item.get('uses'):
              newLines.append("use \\" + use + ";")
              newLines.append("\n")
        newLines.append(line)
        counter+=1
      file.close()
    with open(filename, "w+") as file:
      for line in newLines:
        file.write(line)
      file.close()


ignoreList = ['Util', 'Parameters', 'Resolver', 'Languages', 'None']
mapping = getClassUses(analyzeFolder('src/', ignoreList))

updateFiles(mapping)

